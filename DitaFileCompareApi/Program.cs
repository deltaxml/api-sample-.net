﻿// Copyright (c) 2013 DeltaXML Ltd.  All rights reserved.
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace DitaFileCompareApi
{
    class Program
    {
        /// <summary>
        /// <para>This is the main method of the Program class, which is invoked from the command line.</para>
        /// </summary>
        /// <param name="args">The array of command line arguments.</param>
        static void Main(string[] args)
        {
            if (args.Length > 1)
            { 
                Usage(); 
            }
            else if (args.Length == 1)
            {
                string arg = args[0];

                if (arg == "topic")
                {
                    InitializeDemo();
                    TopicCompare.RunDemo();
                    TerminateDemo();
                }
                else if (arg == "mapfile")
                {
                    InitializeDemo();
                    MapfileCompare.RunDemo();
                    TerminateDemo();
                }
                else if (arg == "mts" || arg == "map-topicset")
                {
                    InitializeDemo();
                    MapTopicsetCompare.RunDemo();
                    TerminateDemo();
                }
                else if (arg == "mts-inplace" || arg == "map-topicset-inplace")
                {
                    InitializeDemo();
                    MapTopicsetCompare.RunDemoInPlace();
                    TerminateDemo();
                }
                else if (arg == "general")
                {
                    InitializeDemo();
                    GeneralCompare.RunDemo();
                    TerminateDemo();
                }
                else
                {
                    Usage();
                }
            }
            else if (args.Length == 0)
            {
                // if no args run all 4 demos
                InitializeDemo();
                TopicCompare.RunDemo();
                MapfileCompare.RunDemo();
                MapTopicsetCompare.RunDemo();
                GeneralCompare.RunDemo();
                TerminateDemo();
            }
        }

        private static void Usage()
        {
            Console.WriteLine("Usage: DitaFileCompareAPI [ topic | mapfile | mts | mts-inplace ]");
        }

        private static void TerminateDemo()
        {
            Console.WriteLine("\r\nDITA Compare Demo Completed.\r\n");
        }

        private static void InitializeDemo()
        {
            Console.WriteLine("\r\nStarting DeltaXML DITA Compare Demo...");

            // remove any pre-existing results directory
            string apiSamplesDir = Program.SamplesChildDir("api-sample");
            DirectoryInfo resultsDir = Directory.CreateDirectory(Path.Combine(apiSamplesDir, "results"));
            if (resultsDir.Exists)
            {
                Console.WriteLine("\r\nDeleting previous results");
                resultsDir.Delete(true);
            }
        }

        internal static string SamplesChildDir(string directoryName)
        {
                // Find the top level samples directory
                DirectoryInfo docdir = new DirectoryInfo(".");
                while (docdir.Exists && docdir.Name != "samples")
                {
                    docdir = docdir.Parent;
                }
                // create subdirectory if it does not yet exist - then return
                return docdir.CreateSubdirectory(directoryName).FullName;
        }

        public static void CopyDirectory(DirectoryInfo source, DirectoryInfo target)
        {
            foreach (DirectoryInfo directory in source.GetDirectories())
                CopyDirectory(directory, target.CreateSubdirectory(directory.Name));
            foreach (FileInfo file in source.GetFiles())
                file.CopyTo(Path.Combine(target.FullName, file.Name));
        }
    }
}

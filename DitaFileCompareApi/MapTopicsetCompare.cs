﻿// Copyright (c) 2013 DeltaXML Ltd.  All rights reserved.
using com.deltaxml.dita;
using DeltaXML.DitaCompareApi;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace DitaFileCompareApi
{
    internal static class MapTopicsetCompare
    {
        /// <summary>
        /// <para>RunDemo is a method invoked from the Program class.
        /// <see cref="Program.Main(string[])"/></para>
        /// <para>It runs the map comparison demo, with a 'unified-map' map result structure.</para>
        /// </summary>
        internal static void RunDemo()
        {
            try
            {
                // Get a fresh comparison object
                DitaMapTopicsetCompareDotNet ditaCompare = new DitaMapTopicsetCompareDotNet();

                // Establish results directory path
                string apiSamplesDir = Program.SamplesChildDir("api-sample");
                DirectoryInfo mapCopyLocation = new DirectoryInfo(Path.Combine(apiSamplesDir, @"results\unified-map"));

                // Choose the unified-map comparison result structure, and set the output location
                // to 'results/unified-map'.
                ditaCompare.MapResultStructure = MapResultStructure.UNIFIED_MAP;

                // Find input directory.
                string mapSamplesDir = Program.SamplesChildDir("topicset-map-sample");
                // Provide variables for each of the file-based compare method arguments
                FileInfo in1 = new FileInfo(Path.Combine(mapSamplesDir, @"doc1\maps\main.ditamap"));
                FileInfo in2 = new FileInfo(Path.Combine(mapSamplesDir, @"doc2\maps\main.ditamap"));

                // Run the compare (with pre/post commentary)
                Console.WriteLine("\r\nComparing files '" + in1.FullName + "' and '" + in2.FullName + "'");
                ditaCompare.compare(in1, in2, mapCopyLocation);
                Console.WriteLine("\r\nStoring results in file '" + Path.Combine(mapCopyLocation.FullName, "result-alias.ditamap") + "'");
            }
            catch (DeltaXMLDitaException e)
            {
                Console.Error.WriteLine(e.Message);
                Console.Error.WriteLine(e.StackTrace);
            }
        }

        /// <summary>
        /// <para>RunDemoInplace is a method invoked from the Program class.
        /// <see cref="Program.Main(string[])"/></para>
        /// <para>It runs the map comparison demo, with a 'topic-set' map result structure.</para>
        /// <para>This demo performs an 'In Place' comparison, so the demo code first clones
        /// the sample directory before invoking the comparison, to avoid tainting the sample.</para>
        /// </summary>
        internal static void RunDemoInPlace()
        {
            try
            {
                // Get a fresh comparison object
                DitaMapTopicsetCompareDotNet ditaCompare = new DitaMapTopicsetCompareDotNet();

                // Establish results directory path
                string apiSamplesDir = Program.SamplesChildDir("api-sample");

                // Choose the topic-set map comparison result structure, and set the output location
                // to 'results/topic-set'.
                ditaCompare.MapResultStructure = MapResultStructure.TOPIC_SET;

                // Find input directory.
                string mapSamplesDir = Program.SamplesChildDir("map-sample");
                string inDir1 = Path.Combine(mapSamplesDir, "doc1");
                string inDir2 = Path.Combine(mapSamplesDir, "doc2");

                // Create a clone of the map-sample directory as a working copy for the in-place comparison,
                // this ensures we don't taint the sample document set
                string mapSamplesCloneDir = Path.Combine(apiSamplesDir, @"results\map-sample-working-copy");
                string outDir1 = Path.Combine(mapSamplesCloneDir, "doc1");
                string outDir2 = Path.Combine(mapSamplesCloneDir, "doc2");
                Program.CopyDirectory(new DirectoryInfo(inDir1), new DirectoryInfo(outDir1));
                Program.CopyDirectory(new DirectoryInfo(inDir2), new DirectoryInfo(outDir2));

                // Provide variables for each of the file-based compare method arguments
                FileInfo in1 = new FileInfo(Path.Combine(mapSamplesCloneDir, @"doc1\maps\main.ditamap"));
                FileInfo in2 = new FileInfo(Path.Combine(mapSamplesCloneDir, @"doc2\maps\main.ditamap"));

                // Run the compare (with pre/post commentary)
                Console.WriteLine("\r\nComparing files (in place) '" + in1.FullName + "' and '" + in2.FullName + "'");
                ditaCompare.compareInPlace(in1, in2);
                Console.WriteLine("\r\nStoring results in place at '" + mapSamplesCloneDir + "'");
            }
            catch (DeltaXMLDitaException e)
            {
                Console.Error.WriteLine(e.Message);
                Console.Error.WriteLine(e.StackTrace);
            }
        }
    }
}

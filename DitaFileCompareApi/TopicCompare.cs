﻿// Copyright (c) 2013 DeltaXML Ltd.  All rights reserved.
using com.deltaxml.dita;
using DeltaXML.DitaCompareApi;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace DitaFileCompareApi
{
    internal static class TopicCompare
    {
        /// <summary>
        /// <para>RunDemo is a method invoked from the Program class.
        /// <see cref="Program.Main(string[])"/></para>
        /// <para>It runs the topic comparison demo.</para>
        /// </summary>
        internal static void RunDemo()
        {
            try
            {
                DitaTopicCompareDotNet ditaCompare = new DitaTopicCompareDotNet();

                // Enable the output indentation option/parameter
                ditaCompare.IndentOutput = IndentOutput.YES;

                // Establish results directory path
                string apiSamplesDir = Program.SamplesChildDir("api-sample");
                string resultsDir = Directory.CreateDirectory(Path.Combine(apiSamplesDir, "results")).FullName;

                // Find input directory.
                string topicSamplesDir = Program.SamplesChildDir("topic-sample");

                // Provide variables for each of the file-based compare method arguments
                FileInfo in1 = new FileInfo(Path.Combine(topicSamplesDir, "in1.dita"));
                FileInfo in2 = new FileInfo(Path.Combine(topicSamplesDir, "in2.dita"));
                FileInfo outFile = new FileInfo(Path.Combine(resultsDir, "markup.dita"));

                // Run the compare (with pre/post commentary)
                Console.WriteLine("\r\nComparing files '" + in1.FullName + "' and '" + in2.FullName + "'");
                ditaCompare.compare(in1, in2, outFile);
                Console.WriteLine("\r\nStoring results in file '" + outFile.FullName + "'");
            }
            catch (FileNotFoundException e)
            {
                Console.Error.WriteLine(e.Message);
                Console.Error.WriteLine(e.StackTrace);
            }
            catch (DeltaXMLDitaException e)
            {
                Console.Error.WriteLine(e.Message);
                Console.Error.WriteLine(e.StackTrace);

            }
        }

    }
}

﻿// Copyright (c) 2013 DeltaXML Ltd.  All rights reserved.
using com.deltaxml.dita;
using DeltaXML.DitaCompareApi;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace DitaFileCompareApi
{
    internal static class GeneralCompare
    {
        /// <summary>
        /// <para>RunDemo is a method invoked from the Program class.
        /// <see cref="Program.Main(string[])"/></para>
        /// <para>It runs a topic comparison and then a map comparison.</para>
        /// <para>The map comparison is performed with a 'map-pair' map result structure.</para>
        /// </summary>
        internal static void RunDemo()
        {
            // Get a fresh comparison object
            DitaCompareDotNet ditaCompare = new DitaCompareDotNet();

            // Establish results directory path
            string apiSamplesDir = Program.SamplesChildDir("api-sample");
            string resultsDir = Directory.CreateDirectory(Path.Combine(apiSamplesDir, "results")).FullName;

            // Choose the map-pair map comparison result structure, and set the output location
            // to 'results/map-pair'. Also specify that the differences within the topics are
            // represented using oXygen's tracked changes format.
            ditaCompare.MapResultStructure = MapResultStructure.MAP_PAIR;
            DirectoryInfo mapCopyLocation = new DirectoryInfo(Path.Combine(resultsDir, "map-pair"));
            ditaCompare.OutputFormat = OutputFormat.OXYGEN_TCS;
            
            // Find input directories
            string topicSamplesDir = Program.SamplesChildDir("topic-sample");
            string mapSamplesDir = Program.SamplesChildDir("topicset-map-sample");
            
            // Provide variables for each of the file-based compare method arguments
            FileInfo inTopic1 = new FileInfo(Path.Combine(topicSamplesDir, "in1.dita"));
            FileInfo inTopic2 = new FileInfo(Path.Combine(topicSamplesDir, "in2.dita"));
            FileInfo outTopic = new FileInfo(Path.Combine(resultsDir, "oxygen-tcs.dita"));
            FileInfo inMap1 = new FileInfo(Path.Combine(mapSamplesDir, @"doc1\maps\main.ditamap"));
            FileInfo inMap2 = new FileInfo(Path.Combine(mapSamplesDir, @"doc2\maps\main.ditamap"));
            FileInfo outMapfile = new FileInfo(Path.Combine(resultsDir, "oxygen-tcs.ditamap"));

            // Run a topic compare (with pre/post commentary)
            Console.WriteLine("\r\nUse the topic comparator to compare two DITA topic files:");
            Console.WriteLine("  " + inTopic1.FullName);
            Console.WriteLine("  " + inTopic2.FullName);
            ditaCompare.compareTopic(inTopic1, inTopic2, outTopic);
            Console.WriteLine("\r\nStoring results in file '" + outTopic.FullName + "'");

            // Run a mapfile map compare (with pre/post commentary)
            Console.WriteLine("\r\nUse the mapfile comparator to compare the XML content of two DITA map files:");
            Console.WriteLine("  " + inMap1.FullName);
            Console.WriteLine("  " + inMap2.FullName);
            ditaCompare.compareMapfile(inMap1, inMap2, outMapfile);
            Console.WriteLine("\r\nStoring results in file '" + outMapfile.FullName + "'");

            // Run a map compare (with pre/post commentary)
            Console.WriteLine("\r\nUse the map-topicset comparator to compare the topics within two DITA map files:");
            Console.WriteLine("  " + inMap1.FullName);
            Console.WriteLine("  " + inMap2.FullName);
            ditaCompare.compareMapTopicset(inMap1, inMap2, mapCopyLocation);
            Console.WriteLine("\r\nStoring results in file '" + Path.Combine(mapCopyLocation.FullName, "result-alias.ditamap") + "'");
        }

    }
}

# API Sample

*The instructions here assume that the sample resources have been checked-out, cloned or downloaded and unzipped into the samples directory of the DITA-Compare release. The resources should be located such that they are two levels below the top level release directory that contains the jar files.*

*For example `DeltaXML-DITA-Compare-8_0_0_j/samples/sample-name`.*

---

## Summary

This sample illustrates how the API can be used to run the DITA Comparison. For concept details see: [API Sample documentation](https://docs.deltaxml.com/dita-compare/latest/api-sample-8290429.html)

In this platform there are four main API classes for accessing the comparison.

| Class | Description |
| --- | --- |
DitaTopicCompare | For accessing the DITA topic comparison.
DitaMapfileCompare | For accessing the DITA map file comparison.
DitaMapTopicsetCompare | For accessing the DITA map based topic-set comparison.
DitaCompare | For accessing the map-file, map-topicset and topic comparisons, through a simplified interface.

## Command-line Arguments

The samples support the use of a command-line argument from the entry-point class to control which DITA Compare API classes are used. The arguments used are included in the sample summary tables.

---

## .NET API

This sample is a .NET console application, DitaFileCompareApi.

There are four C# classes:

| Sample Class | Corresponding API Class | Command-line argument |
| --- | --- | --- |
| TopicCompare.cs | DitaTopicCompareDotNet | topic
| MapfileCompare.cs | DitaMapfileCompareDotNet | mapfile
| MapTopicsetCompare.cs | DitaMapTopicsetCompareDotNet | mts |
| GeneralCompare.cs | DitaCompareDotNet | (empty)
| Program.cs | (none - the startup class) | (not-applicable)

## Using Visual Studio

To open and run the sample in Visual Studio, open the DitaFileCompareApi.csproj project file, select the project in Solution Explorer and press Run or `F5`. Note: command-line arguments can be set as Debug properties in the project properties dialog.

## From the command line (without Visual Studio)

Invoke `rundemo.bat` from the command-line to build the DitaFileCompareApi project using msbuild. The resulting DitaFileCompareApi.exe is then executed along with any command-line arguments supplied when the batch file was invoked.